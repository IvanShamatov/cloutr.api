require 'bundler'
require 'json'
Bundler.require
require 'pp'

# eventmachine which subscribes to messeges and upload files\
class Uploader < Goliath::API

  def awsclient
    Aws.config[:region] = "us-east-1"
    Aws.config[:credentials] = Aws::SharedCredentials.new
    Aws::S3::Client.new
  end


  def response(env)
    bucket = redis.get "#{env["HTTP_KEY"]}:bucket"
    domain = redis.get "#{env["HTTP_KEY"]}:domain"
    tmpfile = env['HTTP_X_FILE']
    extention = File.extname(env["HTTP_FILENAME"])
    name = File.basename(env["HTTP_FILENAME"], extention)
    newname = "#{name}-#{Time.now.to_i.to_s[3..-1]}#{extention}"
    awsclient.put_object({bucket: bucket, body: File.read(tmpfile), key: newname, acl: "public-read"})
    link = "http://#{domain}.cloutr.ee/#{newname}"
    [200, {}, {success: link}.to_json]
  rescue Exception => e
    pp e
    pp e.class
    pp e.backtrace
  end
end