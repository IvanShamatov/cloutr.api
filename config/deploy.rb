# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'api'
set :repo_url, 'git@bitbucket.org:IvanShamatov/cloutr.api.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }.call

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/home/deployer/api'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []
set :linked_dirs, %w{log pids sockets}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
# set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      within current_path do
        Process.kill("INT", File.read("pids/auth.pid").to_i) if File.exist?("pids/auth.pid")
        Process.kill("INT", File.read("pids/uploader.pid").to_i) if File.exist?("pids/uploader.pid")
        execute :bundle, "exec ruby auth.rb -sv -c config/config.rb -e production -S sockets/auth.sock -d -P pids/auth.pid -l log/auth.log"
        execute :bundle, "exec ruby uploader.rb -sv -c config/config.rb -e production -S sockets/uploader.sock -d -P pids/uploader.pid -l log/uploader.log"
      end
    end
  end

  after :publishing, :restart
end
