require 'bundler'
Bundler.require
require 'digest/sha1'
require 'pp'

class Auth < Goliath::API
  def response(env)
    secret = redis.get "#{env["HTTP_KEY"]}:secret"
    string_to_sha = [env["HTTP_KEY"], env["HTTP_TIMESTAMP"], env["HTTP_FILENAME"], secret].join(":")
    status = Digest::SHA1.hexdigest(string_to_sha) == env["HTTP_CHECKSUM"] ? 200 : 401
    [status, {}, nil]
  end
end